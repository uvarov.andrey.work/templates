<?php
/*--------------------
Option page
--------------------*/
	$favicon = get_field('favicon','options');
	$logotype = get_field('logotype','options');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="keywords" content="<?php bloginfo('keywords'); ?>"/>
		<meta name="description" content="<?php bloginfo('description'); ?>"/>
		<meta name="copyright" content="<?php bloginfo('copyright'); ?>">
		<meta content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width" name="viewport">
		<meta name="author" content="QuatroIT">
		<meta name="format-detection" content="telephone=no">
		<link rel="shortcut icon" href="<?= $favicon ?>">
		<meta name="robots" content="nofollow" />
		<!--
				Styles
		-->
		<?php wp_head(); ?>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/default.css">
	</head>
	<body <?php body_class(); ?> class="home">
		