$(document).ready( function() {

	/*-------------------- MENU --------------------*/
	$('.nav-btn').on('click', function(){
		$('.nav-menu').toggle('slow');
		$(this).toggleClass('active');
		return false;
	});
	$('.dropdown').on('click', function(){
		$(this).toggleClass('active');
		$(this).children('ul').toggle('slow');
		return false;
	});
	$('.dropdown2').on('click', function(){
		$(this).toggleClass('active');
		$(this).children('ul').toggle('slow');
		return false;
	});
	/*-------------------- MASK --------------------*/
	$('.user-phone').mask("+7 (999) 999-99-99");
	/*-------------------- MODAL WINDOW	--------------------*/
	$('.popup-open').on('click', function(){
		$('.overlay').addClass('active');
		$('body').addClass('modal');
		$('.popup').removeClass('active');
		rel=$(this).attr('rel');
		$('.popup-'+rel).addClass('active');
		return false;
	});
	$('.popup-close, .overlay').on('click', function(){
		$('.overlay').removeClass('active');
		$('body').removeClass('modal');
		$('.popup').removeClass('active');
	});
});

